import cv2



video_path = '/home/nazanin/Desktop/computer vision for cars/stock-footage-luton-uk-september-evening-traffic-on-british-motorway-m-near-to-junction-ten-and-town.webm'
capturing = cv2.VideoCapture(video_path)

# Desired width and height
desired_width = 640
desired_height = 480

# Get the frame rate of the video
fps = capturing.get(cv2.CAP_PROP_FPS)
delay = int(1000 / fps)

while True:
    ret, frame = capturing.read()
    if not ret:
        break

    # Resize the frame
    resized_frame = cv2.resize(frame, (desired_width, desired_height))

    # Display the resized frame
    cv2.imshow('Video', resized_frame)
    if cv2.waitKey(delay) & 0xFF == ord('q'):
        break

capturing.release()
cv2.destroyAllWindows()
