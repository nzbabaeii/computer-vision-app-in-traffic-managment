import cv2

video_path = '/home/nazanin/Desktop/computer vision for cars/stock-footage-luton-uk-september-evening-traffic-on-british-motorway-m-near-to-junction-ten-and-town.webm'
capturing = cv2.VideoCapture(video_path)
backsub = cv2.createBackgroundSubtractorMOG2()

# Desired width and height
desired_width = 640
desired_height = 480

# Get the frame rate of the video
fps = capturing.get(cv2.CAP_PROP_FPS)

# Check if fps is valid
if fps == 0:
    fps = 30  # Set a default frame rate if fps is zero

delay = int(1000 / fps)

while True:
    ret, frame = capturing.read()
    if not ret:
        break

    resized_frame = cv2.resize(frame, (desired_width, desired_height))

    fgMask = backsub.apply(resized_frame)
    contours, _ = cv2.findContours(fgMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    for contour in contours:
        x, y, w, h = cv2.boundingRect(contour)
        min_area = 1000
        if cv2.contourArea(contour) > min_area:
            cv2.rectangle(resized_frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

    # Display the resized frame
    cv2.imshow('Video', resized_frame)
    if cv2.waitKey(delay) & 0xFF == ord('q'):
        break

capturing.release()
cv2.destroyAllWindows()
